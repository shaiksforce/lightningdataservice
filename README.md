# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This Repository is contains lightning Components and controllers.
the components are created by using the Lightning Data Service Concept
By using Lightning Data Service Concept 
--> No need to write any Apex class
--> No need to write SOQL
--> Field level security and record sharing is inbuilt
--> CRUD operation supported
--> Shared cache is used by all standard and custom components
--> Auto notification to all components
--> Supports offline in Salesforce 1

### How do I get set up? ###
if your using Developer console in salesforce
just open developer console and create Lighning Components and CONTROLLER
Use this components in Lightnig app builder to see the result.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact